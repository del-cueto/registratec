<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::post('/registrar/agregar/carrera', 'HomeController@addCareer');
Route::post('/registrar/agregar/area', 'HomeController@addArea');

Auth::routes();

Route::group(['middleware' => ['auth']], function () {
	Route::get('/', 'HomeController@welcome');

	Route::get('/galeria', 'HomeController@index');
	Route::get('/galerias', 'HomeController@index');
	Route::post('/galeria/subir', 'HomeController@uploadGalleryImage');
	Route::get('/galeria/eliminar/{galeria}', 'HomeController@removeGallery');

	Route::get('/repositorio', 'HomeController@repositories');
	Route::get('/repositorios', 'HomeController@repositories');


	//Route::get('/eventos', 'HomeController@events');
	Route::get('eventos',array('uses' =>'HomeController@events'));
	Route::get('events',array('uses' =>'HomeController@events'));
	Route::post('/eventos/agregar/cancelar/{evento}', 'HomeController@actideactivate');
	Route::post('/eventos/inscribir/cancelar/{evento}', 'HomeController@assigncancel');
	Route::post('/eventos/inscribir', 'HomeController@assignEvent');
	Route::get('/buscar/evento/{evento}', 'HomeController@searchEvent');
	Route::get('/mis/eventos', 'HomeController@myEvents');
	Route::get('/mi/evento/repositorio/{evento}', 'HomeController@repository');
	Route::post('/evento/subir/repositorio/{evento}', 'HomeController@addRepository');
	Route::get('/mi/evento/repositorio/eliminar/{repositorio}', 'HomeController@deleteRepository');
	Route::get('buscar/eventos/{usuario}', 'HomeController@searchEvents');
	Route::get('/eventos/reporte', 'HomeController@eventsReport');
	Route::post('/modificar/evento/{evento}', 'HomeController@modifyEvent');
	Route::get('/borrar/evento/{evento}', 'HomeController@deleteEvent');
	Route::get('/eventos/asistencia', 'HomeController@assistance');
	Route::get('/eventos/asistencia/{evento}', 'HomeController@eventAssistance');

	Route::get('/recursos', 'HomeController@resources');

	Route::get('/registrar', 'HomeController@register');
	Route::get('/registrar/eventos', 'HomeController@viewRegisterEvents');
	Route::post('/registrar/eventos', 'HomeController@registerEvents');
	Route::get('/registrar/asistentes', 'HomeController@viewRegisterAssistants');
	Route::get('/registrar/asistentes/agregar/eliminar/{usuario}', 'HomeController@registerAssistants');
	Route::get('/registrar/usuarios', 'HomeController@viewRegisterUsers');
	Route::get('/registrar/nuevos/usuarios', 'HomeController@viewRegisterNewUsers');
	Route::post('/registrar/nuevos/usuarios', 'HomeController@registerNewUsers');
	Route::get('/registrar/modificar/usuarios', 'HomeController@viewRegisterModifyUsers');
	Route::get('/registrar/modificar/usuario/{usuario}', 'HomeController@viewRegisterModifyUser');
	Route::post('/registrar/modificar/usuario/{usuario}', 'HomeController@registerModifyUser');
	Route::get('/registrar/usuarios/aprobar/rechazar/{usuario}', 'HomeController@registerAproveRejectUser');
	Route::get('/registrar/usuarios/eventos', 'HomeController@viewRegisterUserEvent');
	Route::post('/registrar/usuarios/eventos', 'HomeController@registerUserEvent');

	Route::get('/repositorios', 'HomeController@repositories');
});