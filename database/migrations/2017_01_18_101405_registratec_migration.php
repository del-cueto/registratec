<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RegistratecMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('careers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });
		
        Schema::create('areas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

		if (Schema::hasTable('users') && Schema::hasTable('careers')) {
			Schema::create('user_careers', function (Blueprint $table) {
				$table->increments('id');
				$table->integer('user_id')->unsigned();
				$table->integer('career_id')->unsigned();
				$table->timestamps();
			});
			
			Schema::table('user_careers', function($table) {
				$table->foreign('user_id')
					->references('id')->on('users')
					->onDelete('cascade');
					
				$table->foreign('career_id')
					->references('id')->on('careers')
					->onDelete('cascade');
			});
		}

		if (Schema::hasTable('users') && Schema::hasTable('areas')) {
			Schema::create('user_areas', function (Blueprint $table) {
				$table->increments('id');
				$table->integer('user_id')->unsigned();
				$table->integer('area_id')->unsigned();
				$table->timestamps();
			});
			
			Schema::table('user_areas', function($table) {
				$table->foreign('user_id')
					->references('id')->on('users')
					->onDelete('cascade');
					
				$table->foreign('area_id')
					->references('id')->on('areas')
					->onDelete('cascade');
			});
		}
		
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('street');
            $table->string('street_number')->nullable();
            $table->string('colony')->nullable();
            $table->integer('postal_number')->nullable();
            $table->string('city');
            $table->string('state')->nullable();
            $table->string('country')->nullable();
            $table->integer('phone')->nullable();
            $table->date('date_start');
            $table->date('date_end');
            $table->dateTime('hour_start');
            $table->dateTime('hour_end');
            $table->string('expositor');
            $table->integer('enrolled');
            $table->string('image_name')->nullable();
            $table->string('map_name')->nullable();
            $table->timestamps();
        });

		if (Schema::hasTable('events')) {
			Schema::create('repositories', function (Blueprint $table) {
				$table->increments('id');
				$table->integer('event_id')->unsigned();
				$table->integer('file_name');
				$table->timestamps();
			});
			
			Schema::table('repositories', function($table) {
				$table->foreign('event_id')
					->references('id')->on('events')
					->onDelete('cascade');
			});
		}

		if (Schema::hasTable('users') && Schema::hasTable('events')) {
			Schema::create('user_events', function (Blueprint $table) {
				$table->increments('id');
				$table->integer('user_id')->unsigned();
				$table->integer('event_id')->unsigned();
				$table->timestamps();
			});
			
			Schema::table('user_events', function($table) {
				$table->foreign('user_id')
					->references('id')->on('users')
					->onDelete('cascade');
					
				$table->foreign('event_id')
					->references('id')->on('events')
					->onDelete('cascade');
			});
		}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		if (Schema::hasTable('user_careers')) {
			Schema::table('user_careers', function ($table) {
				$table->dropForeign('user_careers_user_id_foreign');
				$table->dropForeign('user_careers_career_id_foreign');
			});
		}

		if (Schema::hasTable('user_areas')) {
			Schema::table('user_areas', function ($table) {
				$table->dropForeign('user_areas_user_id_foreign');
				$table->dropForeign('user_areas_area_id_foreign');
			});
		}

		if (Schema::hasTable('repositories')) {
			Schema::table('repositories', function ($table) {
				$table->dropForeign('repositories_event_id_foreign');
			});
		}

		if (Schema::hasTable('user_events')) {
			Schema::table('user_events', function ($table) {
				$table->dropForeign('user_events_user_id_foreign');
				$table->dropForeign('user_events_event_id_foreign');
			});
		}

        Schema::dropIfExists('careers');
        Schema::dropIfExists('areas');
        Schema::dropIfExists('user_careers');
        Schema::dropIfExists('user_areas');
        Schema::dropIfExists('events');
        Schema::dropIfExists('repositories');
        Schema::dropIfExists('user_events');
    }
}
