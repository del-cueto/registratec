@extends('layouts.app')

@section('content')
	<div class="container col-md-offset-1 col-md-10">
		<div class="row">
			@if (Auth::user()->role == 0 || Auth::user()->role == 1)
				<div class="col-sm-12" style="color: black;font-size: 26px;margin-top: 10px;">
					Galeria
				</div>
				<div class="col-xs-12 col-sm-6 col-md-4" style="margin-bottom: 20px;">
					<!-- New Gallery -->
					<form action="{{ url('galeria/subir') }}" method="POST" class="form-horizontal" enctype="multipart/form-data" style="border: solid darkgrey 5px;border-radius: 9px;">
						{!! csrf_field() !!}

						<div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
							<label class="col-xs-12 text-center">Selecciona una imagen</label>

							<div class="col-xs-12 text-center">
								<input type="file" class="form-control" id="image" name="image" value="{{ old('image') }}" runat="server" style="border: none;">
								<img class="img-responsive" id="blah" src="#" alt="tu imagen" />

								@if ($errors->has('image'))
									<span class="help-block">
										<strong>{{ $errors->first('image') }}</strong>
									</span>
								@endif
							</div>
							<span>
								{{--base_path('public')--}}
							</span>
						</div>

						<!-- Add Gallery Load Button -->
						<div class="form-group">
							<div class="col-xs-12 text-center">
								<button type="submit" class="btn btn-success">
									<i class="fa fa-plus fa-fw"></i> Agregar
								</button>
							</div>
						</div>
					</form>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-8">
					@if ($galleries)
						@foreach ($galleries as $gallery)
							<div class="col-xs-12 col-sm-6 col-md-4 col-sm-3">
								<div class="img-gallery">
									<a href="/galeria/eliminar/{{$gallery->id}}" class="remove-gallery">X</a>
									<img class="img-responsive" src="{{ '\galeria\\' . $gallery->image_name }}"/>
								</div>
							</div>
						@endforeach
					@endif
				</div>
			@endif
			
			@if (Auth::user()->role > 1)
				<div class="col-md-12" style="color: black;font-size: 26px;margin-top: 10px;">
					Galeria
				</div>
				<div class="col-md-12">
					@if ($galleries)
						@foreach ($galleries as $gallery)
							<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
								<div class="img-gallery">
									<img class="img-responsive" src="{{ '\\galeria\\' . $gallery->image_name }}"/>
								</div>
							</div>
						@endforeach
					@endif
				</div>
			@endif
		</div>
	</div>
@endsection

@section('script')
	<script>
		$(document).ready(function(){
			console.log('ready');
			$('#top_separator').hide();
			
			function readURL(input) {

				if (input.files && input.files[0]) {
					var reader = new FileReader();

					reader.onload = function (e) {
						$('#blah').attr('src', e.target.result);
					}

					reader.readAsDataURL(input.files[0]);
				}
			}

			$("#image").change(function(){
				readURL(this);
			});
		});
	</script>
@endsection
