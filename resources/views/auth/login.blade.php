@extends('layouts.app')

@section('content')
<div class="container col-md-offset-1 col-md-10">
    <div class="row">
        <div class="col-xs-12 col-md-4">
            @include('common.flash')
            <div class="panel panel-default" style="margin-top: 120px">
                <div class="panel-heading text-center" style="background-color: darkgrey; font-size: 20px; color:white">Iniciar sesion</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            {{--<label for="email" class="col-md-4 control-label">E-Mail Address</label>--}}

                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control text-center" name="email" value="{{ old('email') }}" required autofocus placeholder="E-mail">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            {{--<label for="password" class="col-md-4 control-label">Password</label>--}}

                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control text-center" name="password" required placeholder="Contraseña">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : ''}}> Recordar
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Ingresar
                                </button>

                                <a class="btn btn-link" href="{{ url('/password/reset') }}">
                                    Olvidaste tu contraseña?
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
			<a href="/register" class="text-center">
				¿Eres nuevo? Registrate.
			</a>
        </div>
        <div class="col-xs-12 col-md-8" style="margin-top: 50px">
			<p class="text-center" style="font-size: 25px;">
				<strong>RegistraTec</strong> es un sistema para la gesti&oacute;n</br>
				de eventos y conferencias del ITCJ.</br></br>
				Aqui podr&aacute;s registrarte para acceder a</br>
				diferentes eventos de nuestra</br>
				instituci&oacute;n, obtener informaci&oacute; acerca</br>
				de los horarios, fechas, instructores, as&iacute;</br>
				como eventos anteriores en los que haz</br>
				participado.
			</p>
		</div>
    </div>
</div>
@endsection
