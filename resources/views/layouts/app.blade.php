<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
	
	<link rel="stylesheet" href="{{ asset('css/font-awesome-4.7.0/css/font-awesome.min.css') }}">
	
	@yield('style')

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top hidden-xs hidden-sm col-md-offset-1 col-md-10" style="min-height: 110px;z-index: 1001;margin-top: 15px;border-top-left-radius: 10px;border-top-right-radius: 10px;">
            <div class="container col-sm-11">
                <div class="navbar-header">

                    <!-- Branding Image -->
                    <div class="navbar-brand row">
						<a class="hidden-xs" href="{{ url('/') }}">
							{{-- config('app.name', 'Laravel') --}}
							<img class="img-responsive" src="{{ asset('/img/head-img1.png') }}" alt="logo tec"> 
						</a>
                    </div>
                </div>
            </div>
        </nav>
		
		<nav class="navbar navbar-default navbar-static-top col-md-offset-1 col-md-10" style="background-color: darkgrey;border-bottom: solid darkred 15px;color: white;font-size: 15px;min-height: 50px;">
			<div class="container col-md-11">
				@if(!Auth::guest())
					<div class="navbar-header">

						<!-- Collapsed Hamburger -->
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
							<span class="sr-only">Toggle Navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</div>
					
					<div class="collapse navbar-collapse" id="app-navbar-collapse">
						<!-- Left Side Of Navbar -->
						<ul class="nav navbar-nav">
							<li style="padding: 5px 10px; font-size: 20px;">RegistraTEC</li>
							<li><a href="{{ url('/galerias') }}">Galeria</a></li>
							@if (Auth::user()->role < 2)
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
										Eventos <span class="caret"></span>
									</a>

									<ul class="dropdown-menu" role="menu">
										<li><a href="{{ url('/events') }}">Eventos</a></li>
										<li><a href="{{ url('/eventos/asistencia') }}">Asistencia</a></li>
									</ul>
								</li>
							@endif
							@if (Auth::user()->role > 1)
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
										Eventos <span class="caret"></span>
									</a>

									<ul class="dropdown-menu" role="menu">
										<li><a href="{{ url('/events') }}">Eventos</a></li>
										<li><a href="{{ url('/mis/eventos') }}">Mis eventos</a></li>
									</ul>
								</li>
							@endif
							@if (Auth::user()->role == 0 || Auth::user()->role == 1)
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
										Registrar <span class="caret"></span>
									</a>

									<ul class="dropdown-menu" role="menu">
										@if (Auth::user()->role == 0)
											<li><a href="{{ url('/registrar/asistentes') }}">Asistentes</a></li>
										@endif
										<li class="dropdown-submenu">
											<a class="submenu" href="#">Usuarios<span class="caret"></span></a>
											<ul class="dropdown-menu">
												<li><a href="{{ url('/registrar/usuarios') }}">Aprobar</a></li>
												<li><a href="{{ url('/registrar/nuevos/usuarios') }}">Registrar</a></li>
												<li><a href="{{ url('/registrar/modificar/usuarios') }}">Modificar</a></li>
												<li><a href="{{ url('/registrar/usuarios/eventos') }}">Agregar a eventos</a></li>
											</ul>
										</li>
										<li><a href="{{ url('/registrar/eventos') }}">Registrar eventos</a></li>
									</ul>
								</li>
							@endif
							<li><a href="{{ url('/repositorio') }}">Repositorios</a></li>
							@if (Auth::user()->role < 2)
								<li><a href="{{ url('/eventos/reporte') }}">Reporte de eventos</a></li>
							@endif
						</ul>

						<!-- Right Side Of Navbar -->
						<ul class="nav navbar-nav navbar-right">
							<!-- Authentication Links -->
							@if (Auth::guest())
								<li><a href="{{ url('/login') }}">Iniciar sesion</a></li>
								<li><a href="{{ url('/register') }}">Registrarse</a></li>
							@else
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
										{{ Auth::user()->name . " " . Auth::user()->first_last_name }} <span class="caret"></span>
									</a>

									<ul class="dropdown-menu" role="menu">
										<li>
											<a href="{{ url('/logout') }}"
												onclick="event.preventDefault();
														 document.getElementById('logout-form').submit();">
												Cerrar sesion
											</a>

											<form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
												{{ csrf_field() }}
											</form>
										</li>
									</ul>
								</li>
							@endif
						</ul>
					</div>
				@endif
			</div>
		</nav>
		<div class="col-md-offset-1 col-md-10" style="min-height: 15px;background-color: lightgrey;"></div>

        @yield('content')
    </div>
	
	<div class="col-md-offset-1 col-md-10" style="border-bottom: darkgrey solid 60px;border-top: lightgrey solid 20px;min-height: 95px;background-color: darkred;"></div>
	
	<div class="visible-xs col-xs-12 text-center">
		<p>Conmutador (656)688-2500</p>
		<p>Av. Tecnologico No. 1340 C.P. 32500</p>
		<p>Ciudad Juárez. Chih. México</p>
	</div>
	
	<div class="hidden-xs col-xs-10 col-xs-offset-1 text-right" style="background-color: white;border-bottom-left-radius: 10px;border-bottom-right-radius: 10px;margin-bottom: 15px;">
		<p>Conmutador (656)688-2500</p>
		<p>Av. Tecnologico No. 1340 C.P. 32500</p>
		<p>Ciudad Juárez. Chih. México</p>
	</div>
	
	<script
		src="https://code.jquery.com/jquery-3.1.1.js"
		integrity="sha256-16cdPddA6VdVInumRGo6IbivbERE8p7CQR3HzTBuELA="
		crossorigin="anonymous"></script>	

    <!-- Scripts -->
    <script src="{{ asset('/js/app.js') }}"></script>
	@yield('script')
	
	<script>
		$(document).ready(function(){
			$('.dropdown-submenu a.submenu').on("click", function(e){
				$(this).next('ul').toggle();
				e.stopPropagation();
				e.preventDefault();
			});
		});
	</script>
</body>
</html>
