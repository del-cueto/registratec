@extends('layouts.app')

@section('content')
	<div class="container col-md-offset-1 col-md-10">
		<div class="row">
			<div class="col-sm-12" style="color: black;font-size: 26px;margin-top: 10px;">
				Aprobar usuarios
			</div>
			@if ($users)
				<div class="col-xs-12">
					<table class="table table-responsive users-table">
						<!-- Table Headings -->
						<thead style="background-color: darkred;color: white;">
							<th>Nombre</th>
							<th>Apellido Paterno</th>
							<th>Apellido Materno</th>
							<th></th>
							{{--<th></th>--}}
						</thead>
						<!-- Table Body -->
						<tbody>
							@foreach ($users as $user)
								<tr style="background-color: #ff9eaf;box-shadow: inset 0px 5px 16px white;">
									<td>{{ $user->name }}</td>
									<td>{{ $user->first_last_name }}</td>
									<td>{{ $user->second_last_name }}</td>
									@if ($user->status == 0)
										<td>
											<a href="{{ asset('registrar/usuarios/aprobar/rechazar/' . $user->id) }}" style="color: olivedrab; cursor: pointer;">
												<i class="btn-download fa fa-plus fa-2x"></i>
											</a>
										</td>
									@else
										<td>
											<a href="{{ asset('registrar/usuarios/aprobar/rechazar/' . $user->id) }}" style="color: olivedrab; cursor: pointer;">
												<i class="btn-download fa fa-minus fa-2x"></i>
											</a>
										</td>
									@endif
									{{--<td>
										<a href="{{ asset('registrar/usuarios/modificar/' . $user->id) }}" style="color: olivedrab; cursor: pointer;">
											<i class="btn-download fa fa-edit fa-2x"></i>
										</a>
									</td>--}}
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			@endif
		</div>
	</div>
@endsection

@section('script')>
	<script>
		$(document).ready( function(){
			console.log('ready');
		});
	</script>
@endsection
