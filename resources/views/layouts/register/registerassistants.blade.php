@extends('layouts.app')

@section('content')
	<div class="container col-md-offset-1 col-md-10">
		<div class="row">
			<div class="col-sm-12" style="color: black;font-size: 26px;margin-top: 10px;">
				Asignar asistentes
			</div>
			@if ($users)
				<div class="col-xs-12">
					<table class="table table-responsive users-table">
						<!-- Table Headings -->
						<thead style="background-color: darkred;color: white;">
							<th>Nombre</th>
							<th>Correo</th>
							<th>N.Control/Reloj</th>
							<th>Carrera/Area</th>
							<th></th>
						</thead>
						<!-- Table Body -->
						<tbody>
							@foreach ($users as $user)
								<tr style="background-color: #ff9eaf;box-shadow: inset 0px 5px 16px white;">
									<td>{{ $user->name . " " . $user->first_last_name }}</td>
									<td>{{ $user->email }}</td>
									<td>
										@if(isset($user->control_number))
											{{ $user->control_number }}
										@elseif(isset($user->clock_number))
											{{ $user->clock_number }}
										@endif
									</td>
									<td>
										@if(isset($user->career->name))
											carrera
										@elseif(isset($user->area->name))
											area
										@endif
									</td>
									<td>
										@if($user->role != 1)
											<a href="{{ asset('registrar/asistentes/agregar/eliminar/' . $user->id) }}" style="color: olivedrab; cursor: pointer;">
												<i class="btn-download fa fa-plus fa-2x"></i>
											</a>
										@else
											<a href="{{ asset('registrar/asistentes/agregar/eliminar/' . $user->id) }}" style="color: olivedrab; cursor: pointer;">
												<i class="btn-download fa fa-minus fa-2x"></i>
											</a>
										@endif
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			@endif
		</div>
	</div>
@endsection

@section('script')>
	<script>
		$(document).ready( function(){
			console.log('ready');
		});
	</script>
@endsection
