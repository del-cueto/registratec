@extends('layouts.app')

@section('content')
	<div class="container col-md-offset-1 col-md-10">
		<div class="row">
			<div class="col-md-12">
				Galeria
			</div>
			<div class="col-md-12">
				@if ($events)
					@foreach ($events as $event)
						<div class="col-xs-6 col-sm-3">
							<a href="{{ url('/eventos/' . $event->id) }}">
								<img class="img-responsive" src="{{ url($storage_path . '/eventos/img' . $event->image_name) }}"/>
							</a>
						</div>
					@endforeach
				@endif
			</div>
		</div>
	</div>
@endsection

@section('script')
	<script>
		$(document).ready(function(){
				console.log('ready');
				$('#top_separator').hide();
		});
	</script>
@endsection
