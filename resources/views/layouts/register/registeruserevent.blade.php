@extends('layouts.app')

@section('content')
	<style>
		.modal .row > * {
			padding: 5px;
		}
		
		.modal.in .modal-dialog{
			top: 25%;
		}
		
		.modal-body{
			overflow:auto;
			max-height: 420px;
		}
	</style>
	<div class="container col-md-offset-1 col-md-10">
		<div class="row">
			<div class="col-sm-12" style="color: black;font-size: 26px;margin-top: 10px;">
				Agregar a eventos
			</div>
			@if ($users)
				<div class="col-xs-12">
					<table class="table table-responsive users-table">
						<!-- Table Headings -->
						<thead style="background-color: darkred;color: white;">
							<th>Nombre</th>
							<th>Correo</th>
							<th>N.Control/Reloj</th>
							<th>Carrera/Area</th>
							<th></th>
						</thead>
						<!-- Table Body -->
						<tbody>
							@foreach ($users as $user)
								<tr style="background-color: #ff9eaf;box-shadow: inset 0px 5px 16px white;">
									<td>{{ $user->name . " " . $user->first_last_name }}</td>
									<td>{{ $user->email }}</td>
									<td>
										@if($user->control_number)
											{{ $user->control_number }}
										@elseif($user->clock_number)
											{{ $user->clock_number }}
										@endif
									</td>
									<td>
										@if($user->career)
											{{ $user->career }}
										@elseif($user->area)
											{{ $user->area }}
										@endif
									</td>
									<td>
										<div class="btn-edit" href="" style="color: olivedrab; cursor: pointer;">
											<input type="hidden" id="user_id" value="{{ $user->id }}">
											<i class="fa fa-edit fa-2x"></i>
										</div>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			@endif

			<!-- Event Modal -->
			<div class="modal fade" id="eventModal" tabindex="-1" role="dialog" aria-labelledby="eventModalLabel">
			  <div class="modal-dialog" role="document">
				<div class="modal-content">
				  <div class="modal-body">
					<div class="col-xs-12">
						<form class="form-horizontal" id="formAssignEvent" role="form" method="POST" action="/eventos/inscribir">
							{{ csrf_field() }}
							<input type="hidden" name="user_id">
							<input type="hidden" name="event_id">
						</form>
						<table class="table table-responsive events-table">
							<!-- Table Headings -->
							<thead style="background-color: darkred;color: white;">
								<th>Nombre</th>
								<th>Fecha inicio</th>
								<th>Fecha fin</th>
								<th></th>
							</thead>
							<!-- Table Body -->
							<tbody>
							</tbody>
						</table>
					</div>
				  </div>
				</div>
			  </div>
			</div>
		</div>
	</div>
@endsection

@section('script')>
	<script>
		$(document).ready( function(){
			console.log('ready');
			$('.btn-edit').click(function() {
				$('#eventModal').modal('toggle');
				var user_id = $(this).find('#user_id').val();
				$('input[name=user_id]').val(user_id);
				$.ajax({
					headers:{
						'X-CSRF-TOKEN': $('input[name=_token]').val()
					},
					url: '/buscar/eventos/' + user_id,
					type: "get",
					dataType: "json",
					success: function (data) {
						$tr = $('.events-table tbody');
						$.each(data, function (key, item) {
							$.each(item, function (key, value) {
								$tr.prepend(
									$('<tr/>').append($('<td/>', {'class': 'hidden'})
											.append($('<input/>', {type: 'hidden', name: 'event_id', value: value['id']})))
										.append($('<td/>', {'class': 'table-text'})
											.append($('<div/>', {text: value['name']})))
										.append($('<td/>', {'class': 'table-text'})
											.append($('<div/>', {text: value['date_start']})))
										.append($('<td/>', {'class': 'table-text'})
											.append($('<div/>', {text: value['date_end']})))
										.append($('<td/>', {'class': 'table-text'})
											.append($('<button/>', {class:'btn btn-primary assign-event', type:'button', text:'Inscribir', value:value['id']})))
								);
							});
						});
						console.log('eventos de usuario agregados!');
					},
					error: function(data){
						console.log(data.responseJSON['error']);
					}
				});
			});

			$(".events-table").on("click", ".assign-event", function(e) {
				$('input[name=event_id]').val($(this).val());
				$("#formAssignEvent").submit();
			});

			$("#eventModal").on("hidden.bs.modal", function() {
				$('.events-table tbody').find('tr').remove()
			});
		});
	</script>
@endsection