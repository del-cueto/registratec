@extends('layouts.app')

@section('content')
<div class="container col-md-offset-1 col-md-10">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default" style="border-radius: 46px;">
                <div class="panel-heading col-xs-12" style="background-color: transparent;margin-top: 20px;font-size: 24px;">Modificar</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/registrar/modificar/usuario/' . $user->id) }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-12">Modificar la siguiente informaci&oacute;n:</label>

                            <div class="col-md-4">
                                <input id="name" type="text" class="form-control text-center" name="name" required autofocus placeholder="Nombre" value="{{ $user->name }}">

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
							
							<div class="col-md-4">
                                <input id="first_last_name" type="text" class="form-control text-center" name="first_last_name" value="{{ $user->first_last_name }}" required autofocus placeholder="Apellido paterno">

                                @if ($errors->has('first_last_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('first_last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
							
                            <div class="col-md-4">
                                <input id="second_last_name" type="text" class="form-control text-center" name="second_last_name" value="{{ $user->second_last_name }}" autofocus placeholder="Apellido materno">

                                @if ($errors->has('second_last_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('second_last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('role') ? ' has-error' : '' }} col-xs-6">
                            <label for="role" class="col-md-4 control-label">Rol:</label>

                            <div class="col-md-8">
                                <select id="role" type="text" class="form-control" name="role">
									<option value="2">Alumno</option>
									<option value="3">Docente</option>
									<option value="4">Visitante</option>
								</select>
                            </div>
							
                            <div class="col-md-12">
                                <input id="institute_mask" type="text" class="form-control" name="institute_mask" required autofocus placeholder="Institución o Empresa" value="Instituto Tecnológico de Ciudad Juárez" disabled="">
								<input id="institute" type="text" class="form-control hidden" name="institute">

                                @if ($errors->has('institute'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('institute') }}</strong>
                                    </span>
                                @endif
                            </div>
							
                            <div class="col-md-12">
                                <input id="control_number" type="text" class="form-control" name="control_number" value="{{ $user->control_number }}" autofocus placeholder="N&uacute;mero de control">

                                @if ($errors->has('control_number'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('control_number') }}</strong>
                                    </span>
                                @endif
                            </div>
							
                            <div class="col-md-12">
                                <input id="clock_number" type="text" class="form-control" name="clock_number" value="{{ $user->clock_number }}" autofocus placeholder="N&uacute;mero de reloj" style="display: none;">

                                @if ($errors->has('clock_number'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('clock_number') }}</strong>
                                    </span>
                                @endif
                            </div>
							
                            <div class="col-md-12">
                                <input id="ine_id" type="text" class="form-control" name="ine_id" value="{{ $user->ine_id }}" autofocus placeholder="ID IFE" style="display: none;">

                                @if ($errors->has('ine_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('control_number') }}</strong>
                                    </span>
                                @endif
                            </div>
							
                            <div class="col-md-12">
                                <select id="career" type="text" class="form-control" name="career" autofocus>
									<option value="0">Selecciona tu carrera</option>
									@if (isset($careers))
										@foreach ($careers as $career)
											<option value="{{ $career->id }}">{{ $career->name }}</option>
										@endforeach
									@endif
								</select>
                            </div>
							
                            <div class="col-md-12">
                                <select id="area" type="text" class="form-control" name="area" autofocus style="display: none;">
									<option value="0">Area</option>
									@if (isset($areas))
										@foreach ($areas as $area)
											<option value="{{ $area->id }}">{{ $area->name }}</option>
										@endforeach
									@endif
								</select>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} col-xs-6">
                            <label for="email" class="col-md-12">Datos de la cuenta:</label>
							
                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control" name="password" required placeholder="Contraseña">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
							
                            <div class="col-md-12">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Modificar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')>
	<script>
		function roleCheckUp() {
			$('#career').val(0);
			$('#area').val(0);
			if ($('#role').val() == 2) {
				$('#institute_mask').prop('disabled', true)
				$('#institute_mask').val('Instituto Tecnológico de Ciudad Juárez');
				$('#control_number').show();
				$('#clock_number').hide();
				$('#ine_id').hide();
				$('#career').show();
				$('#area').hide();
			} else if ($('#role').val() == 3) {
				$('#institute_mask').prop('disabled', true)
				$('#institute_mask').val('Instituto Tecnológico de Ciudad Juárez');
				$('#control_number').hide();
				$('#clock_number').show();
				$('#ine_id').hide();
				$('#career').hide();
				$('#area').show();
			} else if ($('#role').val() == 4) {
				$('#institute_mask').prop('disabled', false)
				$('#institute_mask').val('');
				$('#control_number').hide();
				$('#clock_number').hide();
				$('#ine_id').show();
				$('#career').hide();
				$('#area').show();
			}
			$('#institute').val($('#institute_mask').val());
		}
		
		$(document).ready( function(){
			console.log('ready');
			
			roleCheckUp();
			
			$('#role').on('change', function(){
				roleCheckUp();
			});
		});
	</script>
@endsection
