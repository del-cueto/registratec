@extends('layouts.app')

@section('content')
	<div class="container col-md-offset-1 col-md-10">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default" style="border-radius: 46px;">
					<div class="panel-heading col-xs-12" style="background-color: transparent;margin-top: 20px;font-size: 24px;">Registro de eventos</div>
					<div class="panel-body">
						<form class="form-horizontal" role="form" method="POST" action="{{ url('/registrar/eventos') }}" enctype="multipart/form-data">
							{{ csrf_field() }}

							<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">

								<div class="col-xs-12">
									<input id="name" type="text" class="form-control text-center" name="name" value="{{ old('name') }}" required autofocus placeholder="Nombre del evento">

									@if ($errors->has('name'))
										<span class="help-block">
											<strong>{{ $errors->first('name') }}</strong>
										</span>
									@endif
								</div>
							</div>

							<div class="form-group{{ $errors->has('colony') ? ' has-error' : '' }} col-xs-12">
								<label for="colony" class="hidden-xs col-xs-0 col-sm-2 control-label">Lugar:</label>
								
								<div class="col-xs-6 col-sm-7">
									<input id="street" type="text" class="form-control text-center" name="street" value="{{ old('street') }}" required autofocus placeholder="Calle">

									@if ($errors->has('street'))
										<span class="help-block">
											<strong>{{ $errors->first('street') }}</strong>
										</span>
									@endif
								</div>
								
								<div class="col-xs-6 col-sm-3">
									<input id="street_number" type="text" class="form-control text-center" name="street_number" value="{{ old('street_number') }}" autofocus placeholder="Numero">

									@if ($errors->has('street_number'))
										<span class="help-block">
											<strong>{{ $errors->first('street_number') }}</strong>
										</span>
									@endif
								</div>
								
								<div class="col-xs-6 col-sm-5">
									<input id="colony" type="text" class="form-control" name="colony" required autofocus placeholder="Colonia">

									@if ($errors->has('colony'))
										<span class="help-block">
											<strong>{{ $errors->first('colony') }}</strong>
										</span>
									@endif
								</div>
								
								<div class="col-xs-6 col-sm-2">
									<input id="postal_number" type="text" class="form-control" name="control_number" value="{{ old('postal_number') }}" autofocus placeholder="Codigo Postal">

									@if ($errors->has('postal_number'))
										<span class="help-block">
											<strong>{{ $errors->first('postal_number') }}</strong>
										</span>
									@endif
								</div>
								
								<div class="col-xs-6 col-sm-5">
									<input id="city" type="text" class="form-control" name="city" value="{{ old('city') }}" autofocus placeholder="Ciudad">

									@if ($errors->has('city'))
										<span class="help-block">
											<strong>{{ $errors->first('city') }}</strong>
										</span>
									@endif
								</div>
								
								<div class="col-xs-6 col-sm-4">
									<input id="state" type="text" class="form-control" name="state" value="{{ old('state') }}" autofocus placeholder="Estado">

									@if ($errors->has('state'))
										<span class="help-block">
											<strong>{{ $errors->first('state') }}</strong>
										</span>
									@endif
								</div>
								
								<div class="col-xs-6 col-sm-4">
									<input id="country" type="text" class="form-control" name="country" value="{{ old('country') }}" autofocus placeholder="Pais">

									@if ($errors->has('country'))
										<span class="help-block">
											<strong>{{ $errors->first('country') }}</strong>
										</span>
									@endif
								</div>
								
								<div class="col-xs-6 col-sm-4">
									<input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}" autofocus placeholder="Telefono">

									@if ($errors->has('phone'))
										<span class="help-block">
											<strong>{{ $errors->first('phone') }}</strong>
										</span>
									@endif
								</div>
								
								<div class="col-xs-4 col-sm-3">
									<input id="date_start" type="text" class="form-control" name="date_start" value="{{ old('date_start') }}" autofocus placeholder="Dia de inicio">

									@if ($errors->has('date_start'))
										<span class="help-block">
											<strong>{{ $errors->first('date_start') }}</strong>
										</span>
									@endif
								</div>
								
								<div class="col-xs-4 col-sm-3">
									<input id="date_end" type="text" class="form-control" name="date_end" value="{{ old('date_end') }}" autofocus placeholder="Dia de fin">

									@if ($errors->has('date_end'))
										<span class="help-block">
											<strong>{{ $errors->first('date_end') }}</strong>
										</span>
									@endif
								</div>
								
								<div class="col-xs-4 col-sm-3">
									<input id="hour_start" type="text" class="form-control" name="hour_start" value="{{ old('hour_start') }}" autofocus placeholder="Hora de inicio">

									@if ($errors->has('hour_start'))
										<span class="help-block">
											<strong>{{ $errors->first('hour_start') }}</strong>
										</span>
									@endif
								</div>
								
								<div class="col-xs-4 col-sm-3">
									<input id="hour_end" type="text" class="form-control" name="hour_end" value="{{ old('hour_end') }}" autofocus placeholder="Hora de fin">

									@if ($errors->has('hour_end'))
										<span class="help-block">
											<strong>{{ $errors->first('hour_end') }}</strong>
										</span>
									@endif
								</div>
								
								<div class="col-xs-6 col-sm-5">
									<input id="category" type="text" class="form-control" name="category" value="{{ old('category') }}" autofocus placeholder="Categoria">

									@if ($errors->has('category'))
										<span class="help-block">
											<strong>{{ $errors->first('category') }}</strong>
										</span>
									@endif
								</div>
								
								<div class="col-xs-6 col-sm-5">
									<input id="expositor" type="text" class="form-control" name="expositor" value="{{ old('expositor') }}" autofocus placeholder="Expositor">

									@if ($errors->has('expositor'))
										<span class="help-block">
											<strong>{{ $errors->first('expositor') }}</strong>
										</span>
									@endif
								</div>
								
								<div class="col-xs-6 col-sm-2">
									<input id="enrolled" type="text" class="form-control" name="enrolled" value="{{ old('enrolled') }}" autofocus placeholder="Cupo">

									@if ($errors->has('enrolled'))
										<span class="help-block">
											<strong>{{ $errors->first('enrolled') }}</strong>
										</span>
									@endif
								</div>
								
								<div class="col-xs-12 col-sm-6  text-center">
									<label class="col-xs-12text-center">Selecciona una imagen</label>
									
									<input type="file" class="form-control" id="image_name" name="image_name" value="{{ old('image_name') }}" runat="server" style="border: none;">
									<div class="img-image">
										<img class="img-responsive" id="image_img" src="#" alt="Imagen del evento" />
									</div>

									@if ($errors->has('image_name'))
										<span class="help-block">
											<strong>{{ $errors->first('image_name') }}</strong>
										</span>
									@endif
								</div>
								
								<div class="col-xs-12 col-sm-6 text-center">
									<label class="col-xs-12 text-center">Selecciona un mapa</label>
									
									<input type="file" class="form-control" id="map_name" name="map_name" value="{{ old('map_name') }}" runat="server" style="border: none;">
									<div class="img-image">
										<img class="img-responsive" id="map_img" src="#" alt="Mapa del evento" />
									</div>

									@if ($errors->has('map_name'))
										<span class="help-block">
											<strong>{{ $errors->first('map_name') }}</strong>
										</span>
									@endif
								</div>
							</div>

							<div class="form-group">
								<div class="col-md-6 col-md-offset-4">
									<button type="submit" class="btn btn-primary">
										Registrar
									</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('script')>
	<script>
		function roleCheckUp() {
			$('#control_number').val('');
			$('#clock_number').val('');
			$('#ine_id').val('');
			$('#career').val(0);
			$('#area').val(0);
			if ($('#role').val() == 2) {
				$('#institute_mask').prop('disabled', true)
				$('#institute_mask').val('Instituto Tecnológico de Ciudad Juárez');
				$('#control_number').show();
				$('#clock_number').hide();
				$('#ine_id').hide();
				$('#career').show();
				$('#area').hide();
			} else if ($('#role').val() == 3) {
				$('#institute_mask').prop('disabled', true)
				$('#institute_mask').val('Instituto Tecnológico de Ciudad Juárez');
				$('#control_number').hide();
				$('#clock_number').show();
				$('#ine_id').hide();
				$('#career').hide();
				$('#area').show();
			} else if ($('#role').val() == 4) {
				$('#institute_mask').prop('disabled', false)
				$('#institute_mask').val('');
				$('#control_number').hide();
				$('#clock_number').hide();
				$('#ine_id').show();
				$('#career').hide();
				$('#area').show();
			}
			$('#institute').val($('#institute_mask').val());
		}
		
		function imageReadURL(input) {

			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function (e) {
					$('#image_img').attr('src', e.target.result);
				}

				reader.readAsDataURL(input.files[0]);
			}
		}
		
		function mapReadURL(input) {

			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function (e) {
					$('#map_img').attr('src', e.target.result);
				}

				reader.readAsDataURL(input.files[0]);
			}
		}
		
		$(document).ready( function(){
			console.log('ready');
			
			$('#top_separator').hide();
			
			roleCheckUp();
			
			$('#role').on('change', function(){
				roleCheckUp();
			});

			$("#image_name").change(function(){
				imageReadURL(this);
			});
			
			$("#map_name").change(function(){
				mapReadURL(this);
			});
		});
	</script>
@endsection
