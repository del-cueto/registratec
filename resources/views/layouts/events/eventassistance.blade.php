@extends('layouts.app')

@section('style')
	<style>
		.btn {
			margin-top: 5px;
			margin-bottom: 5px;
		}
	</style>
@endsection

@section('content')
	<div class="container col-md-offset-1 col-md-10">
		<div class="row">
			<div class="col-xs-12 col-sm-6" style="color: black;font-size: 26px; margin-top: 10px;">
				{{ $event->name }}
			</div>
			<div class="col-xs-12 col-sm-6 assistance-search" style="color: black; margin-top: 10px;">
				<form class="form-horizontal" role="form">
					<div class="form-group">
						<label for="control-number" class="control-label col-xs-3">No. Control: </label>
						<div class="col-xs-8">
							<input id="control-number" type="text" class="form-control" name="control-number">
						</div>
					</div>
					<div class="form-group">
						<label for="email" class="control-label col-xs-3">E-mail: </label>
						<div class="col-xs-8">
							<input id="email" type="email" class="form-control" name="email">
						</div>
					</div>
				</form>
			</div>
			@if ($user_events)
				<div class="col-xs-12 col-md-9">
					<table class="table table-responsive event-assistance-table">
						<!-- Table Headings -->
						<thead style="background-color: darkred;color: white;">
							<th></th>
							<th>Nombre</th>
							<th>Email</th>
							<th>Asistencia</th>
						</thead>
						<!-- Table Body -->
						<tbody>
							@php
								$i = 0
							@endphp
							@foreach ($user_events as $usr_evnt)
								<tr style="background-color: #ff9eaf;box-shadow: inset 0px 5px 16px white;">
									<td class="hidden">
										<input type="hidden" class="user-id" value="{{ $usr_evnt->user->id}}">
									</td>
									<td>{{ $i += 1 }}</td>
									<td>{{ $usr_evnt->user->name }}</td>
									<td>{{ $usr_evnt->user->email }}</td>
									<td class="assistance"></td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			@endif
		</div>
	</div>
@endsection

@section('script')
	<script>
		$(document).ready(function(){
			console.log('ready');
			
			$('.btn-eliminar').click(function(e) {
				window.location = "/mi/evento/repositorio/eliminar/" + $(this).val()
			});
		});
	</script>
@endsection
