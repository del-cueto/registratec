@extends('layouts.app')

@section('style')
	<style>
		.btn {
			margin-top: 5px;
			margin-bottom: 5px;
		}
	</style>
@endsection

@section('content')
	<div class="container col-md-offset-1 col-md-10">
		<div class="row">
			<div class="col-sm-12" style="color: black;font-size: 26px;margin-top: 10px;">
				Eventos de usuarios
			</div>
			@if ($users)
				@foreach ($users as $user)
					@if ($user->user_events->all())
						<div class="col-xs-12">
							<table class="table table-responsive my-events-table">
								<!-- Table Headings -->
								<thead style="background-color: darkred;color: white;">
									<th>{{ $user->name . " " . $user->first_last_name }}</th>
									<th>Evento</th>
									<th>Categoria</th>
									<th>Expositor</th>
									<th>Fecha inicio</th>
									<th>Fecha fin</th>
									<th>Horario inicio</th>
									<th>Horario fin</th>
								</thead>
								<!-- Table Body -->
								<tbody>
									@foreach ($user->user_events as $event)
										<tr class="my-event" style="background-color: #ff9eaf;box-shadow: inset 0px 5px 16px white;">
											<td class="hidden"><input type="hidden" class="event" value="{{ $event->id }}"></td>
											<td></td>
											<td>{{ $event->event->name }}</td>
											<td>{{ $event->event->category }}</td>
											<td>{{ $event->event->expositor }}</td>
											<td>{{ $event->event->date_start }}</td>
											<td>{{ $event->event->date_end }}</td>
											<td>{{ $event->event->hour_start }}</td>
											<td>{{ $event->event->hour_end }}</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					@endif
				@endforeach
			@endif
		</div>
	</div>
@endsection

@section('script')
	<script>
		$(document).ready(function(){
			console.log('ready');
		});
	</script>
@endsection
