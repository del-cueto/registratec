@extends('layouts.app')

@section('style')
	<style>
		.btn {
			margin-top: 5px;
			margin-bottom: 5px;
		}
	</style>
@endsection

@section('content')
	<div class="container col-md-offset-1 col-md-10">
		<div class="row">
			<div class="col-sm-12" style="color: black;font-size: 26px;margin-top: 10px;">
				Eventos
			</div>
			@if ($events)
				<div class="col-xs-12">
					<table class="table table-responsive my-events-table">
						<!-- Table Headings -->
						<thead style="background-color: darkred;color: white;">
							<th>Evento</th>
							<th>Categoria</th>
							<th>Expositor</th>
							<th>Fecha inicio</th>
							<th>Fecha fin</th>
							<th>Horario inicio</th>
							<th>Horario fin</th>
						</thead>
						<!-- Table Body -->
						<tbody>
							@foreach ($events as $event)
								<tr class="my-event" style="background-color: #ff9eaf;box-shadow: inset 0px 5px 16px white;cursor: pointer;">
									<td class="hidden"><input type="hidden" class="event" value="{{ $event->id }}"></td>
									<td>{{ $event->name }}</td>
									<td>{{ $event->category }}</td>
									<td>{{ $event->expositor }}</td>
									<td>{{ $event->date_start }}</td>
									<td>{{ $event->date_end }}</td>
									<td>{{ $event->hour_start }}</td>
									<td>{{ $event->hour_end }}</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			@endif
			<div class="col-sm-12" style="color: black;font-size: 26px;margin-top: 10px;">
				Eventos a los que he asistido
			</div>
			@if ($assisted_events)
				<div class="col-xs-12">
					<table class="table table-responsive my-events-table">
						<!-- Table Headings -->
						<thead style="background-color: darkred;color: white;">
							<th>Evento</th>
							<th>Categoria</th>
							<th>Expositor</th>
							<th>Fecha inicio</th>
							<th>Fecha fin</th>
							<th>Horario inicio</th>
							<th>Horario fin</th>
						</thead>
						<!-- Table Body -->
						<tbody>
							@foreach ($assisted_events as $event)
								<tr class="my-event" style="background-color: #ff9eaf;box-shadow: inset 0px 5px 16px white;cursor: pointer;">
									<td class="hidden"><input type="hidden" class="event" value="{{ $event->id }}"></td>
									<td>{{ $event->name }}</td>
									<td>{{ $event->category }}</td>
									<td>{{ $event->expositor }}</td>
									<td>{{ $event->date_start }}</td>
									<td>{{ $event->date_end }}</td>
									<td>{{ $event->hour_start }}</td>
									<td>{{ $event->hour_end }}</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			@endif
		</div>
	</div>
@endsection

@section('script')
	<script>
		$(document).ready(function(){
			console.log('ready');
			
			$('.my-event').click(function() {
				window.location = "/mi/evento/repositorio/" + $(this).find('.event').val();
			});
		});
	</script>
@endsection
