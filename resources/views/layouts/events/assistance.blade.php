@extends('layouts.app')

@section('style')
	<style>
		.btn {
			margin-top: 5px;
			margin-bottom: 5px;
		}
	</style>
@endsection

@section('content')
	<div class="container col-md-offset-1 col-md-10">
		<div class="row">
			<div class="col-sm-12" style="color: black;font-size: 26px;margin-top: 10px;">
				Asistencia de Eventos
			</div>
			@if ($events)
				<div class="col-xs-12">
					<table class="table table-responsive">
						<!-- Table Headings -->
						<thead>
							<tr style="border: none;">
								<th style="border: none;"></th>
								<th style="border: none;"></th>
							</tr>
						</thead>
						<!-- Table Body -->
						<tbody>
							@php $i = 0 @endphp
							@foreach ($events as $event)
								<tr style="border: none;">
									<td>{{ $i += 1 }}</td>
									<td><a href="{{ '/eventos/asistencia/' . $event->id }}">{{ $event->name }}</a></td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			@endif
		</div>
	</div>
@endsection

@section('script')
	<script>
		$(document).ready(function(){
			console.log('ready');
		});
	</script>
@endsection
