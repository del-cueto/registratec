@extends('layouts.app')

@section('style')
	<style>
		.btn {
			margin-top: 5px;
			margin-bottom: 5px;
		}
	</style>
@endsection

@section('content')
	<div class="container col-md-offset-1 col-md-10">
		<div class="row">
			<div class="col-sm-12" style="color: black;font-size: 26px;margin-top: 10px;">
				Eventos
			</div>
			<div class="hidden-xs col-sm-5 col-md-3" style="height: 50em;">
				<h2 class="col-xs-12 text-left">Lugar</h2>
				<h4 class="col-xs-12 text-left calle">Calle</h4>
				<h5 class="col-xs-12 text-left colonia">Colonia</h5>
				<h5 class="col-xs-12 text-left ciudad">ciudad</h5>
				<h5 class="col-xs-12 text-left telefono">telefono</h5>
				
				<div style="border: solid darkgrey 5px;" class="col-xs-12">
						<img src="#" alt="Mapa" id="" class="img-responsive text-center mapa">
				</div>
			</div>
			@if ($events)
				<div class="col-xs-12 col-sm-7 col-md-9">
					@foreach ($events as $event)
						<div class="col-xs-12 event-panel">
							<form class="form-horizontal" role="form" method="POST" action="{{ url('/modificar/evento/' . $event->id) }}" enctype="multipart/form-data">
								{{ csrf_field() }}
								<div class="hidden-xs col-xs-3 col-sm-3" style="padding: 0px; z-index: 1;">
									<div class="img-gallery" style="border: solid darkgrey 5px;border-radius: 9px;height: 15.5vw;background-color: white;outline:none;">
										<img class="img-responsive" src="{{ '\eventos\\' . $event->image_name }}"/>
										<input type="file" class="form-control" id="image" name="image" value="{{ old('image') }}" runat="server" style="border: none; display: none;">
									</div>
								</div>
								<div class="col-xs-12 col-sm-9" style="padding: 0px;margin-left: -10px;z-index: 0;">
									<div class="panel panel-default">
										<div class="panel-heading" style="background-color: #b4b4b4;font-weight: 900;">
											<span class="event_name">
												{{ $event->name }}
											</span>
											<span class="event_category" style="padding-left: 15em;">
												{{ $event->category }}
											</span>
											<input type="text" name="name" value="{{ $event->name }}" style="display: none;">
											<input type="text" name="category" value="{{ $event->category }}" style="display: none; margin-left: 10em">
										</div>

										<div class="panel-body">
											<div class="delete-event">
												<a href="{{ 'borrar/evento/' . $event->id }}">x</a>
											</div>
											<div class="col-xs-6 col-md-4 event_info1">
												<p>{{ "Inicio: " . $event->date_start }}</p>
												<p>{{ "Fin: " . $event->date_end }}</p>
												<p>{{ "Cupo: " . $event->enrolled }}</p>
												<input type="text" name="date_start" value="{{ $event->date_start }}" style="display: none;">
												<input type="text" name="date_end" value="{{ $event->date_end }}" style="display: none;">
												<input type="text" name="enrolled" value="{{ $event->enrolled }}" style="display: none;">
											</div>
											<div class="col-xs-6 col-md-4 event_info2">
												<p class="hiddable">{{ "Expositor:" . $event->expositor }}</p>
												<p class="hiddable">{{ "De: " . $event->hour_start }}</p>
												<p class="hiddable">{{ "A: " . $event->hour_end }}</p>
												<input type="text" name="expositor" value="{{ $event->expositor }}" style="display: none;">
												<input type="text" name="hour_start" value="{{ $event->hour_start }}" style="display: none;">
												<input type="text" name="hour_end" value="{{ $event->hour_end }}" style="display: none;">
												<p>{{ "Inscritos: " . count($event->assigned) }}</p>
											</div>
											<div class="col-xs-12 col-md-4 text-center">
												@if (Auth::user()->role > 1)
													<div class="col-md-12">
														<button type="button" class="@if ($event->assigned) hidden @endif btn btn-primary btn-inscribir" value ="{{ $event->id }}">
															Inscribirme
														</button>
													</div>
													<div class="col-md-12">
														<button type="button" class="@if (!$event->assigned ) hidden @endif btn btn-primary btn-cancelar" value ="{{ $event->id }}">
															Cancelar
														</button>
													</div>
												@endif
												@if (Auth::user()->role < 2)
													<div class="col-md-12">
														<button type="button" class="@if ($event->status == 1) hidden @endif btn btn-primary btn-activar" value ="{{ $event->id }}">
															Agregar
														</button>
													</div>
													<div class="col-md-12">
														<button type="button" class="@if ($event->status == 0) hidden @endif btn btn-primary btn-desactivar" value ="{{ $event->id }}">
															Cancelar
														</button>
													</div>
													<div class="col-md-12">
														<button type="button" class="btn btn-primary btn-repositorio" value = "{{ $event->id }}">
															Repositorio
														</button>
													</div>
													<div class="col-md-12">
														<button type="button" class="btn btn-primary btn-modificar" value = "{{ $event->id }}">
															Modificar
														</button>
													</div>
												@endif
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					@endforeach
				</div>
			@endif
		</div>
	</div>
@endsection

@section('script')
	<script>
		$(document).ready(function(){
			console.log('ready');
			$('#top_separator').hide();
			
			$('.btn-activar, .btn-desactivar').click(function(e) {
				$event = $(this);
				$.ajax({
					headers:{
						'X-CSRF-TOKEN': $('input[name=_token]').val()
					},
					url: '/eventos/agregar/cancelar/' + $(this).val(),
					type: "post",
					dataType: "json",
					data: {
						'evento': $(this).val(),
					},
					success: function (data) {
						if (data['success']) {
							$event.addClass('hidden');
							if ($event.hasClass('btn-activar')) {
								$event.closest('div').parent().find('.btn-desactivar').removeClass('hidden');
							} else if ($event.hasClass('btn-desactivar')) {
								$event.closest('div').parent().find('.btn-activar').removeClass('hidden');
							}
						} else {
							
						}
					},
					error: function(data) {
						console.log(data['error']);
					}
				});
			});
			
			$('.btn-inscribir, .btn-cancelar').click(function(e) {
				$event = $(this);
				$.ajax({
					headers:{
						'X-CSRF-TOKEN': $('input[name=_token]').val()
					},
					url: '/eventos/inscribir/cancelar/' + $(this).val(),
					type: "post",
					dataType: "json",
					data: {
						'evento': $(this).val(),
					},
					success: function (data) {
						if (data['success']) {
							$event.addClass('hidden');
							if ($event.hasClass('btn-inscribir')) {
								$event.closest('div').parent().find('.btn-cancelar').removeClass('hidden');
							} else if ($event.hasClass('btn-cancelar')) {
								$event.closest('div').parent().find('.btn-inscribir').removeClass('hidden');
							}
						} else {
							
						}
					},
					error: function(data) {
						console.log(data['error']);
					}
				});
			});
			
			$('.btn-repositorio').click(function(e) {
				window.location = "/mi/evento/repositorio/" + $(this).val()
			});
			
			$('.btn-modificar').click(function(e) {
				if ($(this).hasClass('save')) {
					$(this).removeClass('save');
					$(this).text('Modificar')
					$(this).closest('.event-panel').find('#image, .event_name, .event_category, .event_info1 p, .event_info2 p.hiddable').show();
					$(this).closest('.event-panel').find('.img-gallery img').show();
					$(this).closest('.event-panel').find('input').hide();
					$(this).closest('form').submit();
				} else {
					$(this).addClass('save');
					$(this).text('Guardar')
					$(this).closest('.event-panel').find('#image, .event_name, .event_category, .event_info1 p, .event_info2 p.hiddable').hide();
					$(this).closest('.event-panel').find('input').show();
					$(this).closest('.event-panel').find('.img-gallery img').hide();
				}
				//submit with modified values
			});
			
			$('.event-panel').click(function(e) {
				if (e.target.localName != "input") {
					if ($(this).hasClass('event-selected')) {
						$(this).removeClass('event-selected');
						$('.event-panel').removeClass('event-selected')
					} else {
						$('.event-panel').removeClass('event-selected')
						$(this).addClass('event-selected');
						
						$.ajax({
							headers:{
								'X-CSRF-TOKEN': $('input[name=_token]').val()
							},
							url: '/buscar/evento/' + $(this).find('.btn-activar, .btn-inscribir').val(),
							type: "get",
							dataType: "json",
							success: function (data) {
								$('.calle').html(data['street'] + " " + data['street_number']);
								$('.colonia').html(data['colony']);
								$('.ciudad').html(data['city'] + ", " + data['state'] + ", " + data['country']);
								$('.telefono').html(data['phone']);
								$('.mapa').prop('src', "eventos/" + data['map_name']);
							},
							error: function(data) {
								console.log(data['error']);
							}
						});
					}
				}
			});
		});
	</script>
@endsection
