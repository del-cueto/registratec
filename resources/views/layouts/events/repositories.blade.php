@extends('layouts.app')

@section('style')
	<style>
		.btn {
			margin-top: 5px;
			margin-bottom: 5px;
		}
	</style>
@endsection

@section('content')
	<div class="container col-md-offset-1 col-md-10">
		<div class="row">
			<div class="col-sm-12" style="color: black;font-size: 26px;margin-top: 10px;">
				Repositorio
			</div>
			<div class="col-xs-12 col-md-3" style="color: black; margin-top: 50px;">
				<form method="get">
					<h4>Buscar por:</h4>
					<input type="text" name="search">
				</form>
			</div>
			@if ($repositories)
				<div class="col-xs-12 col-md-9">
					<table class="table table-responsive my-events-table">
						<!-- Table Headings -->
						<thead style="background-color: darkred;color: white;">
							<th>Documento</th>
							<th>Categoria</th>
							<th>Expositor</th>
							<th>Fecha</th>
							<th></th>
							@if (Auth::user()->role < 2 )
								<th></th>
							@endif
						</thead>
						<!-- Table Body -->
						<tbody>
							@foreach ($repositories as $repository)
								<tr style="background-color: #ff9eaf;box-shadow: inset 0px 5px 16px white;">
									<td>{{ $repository->file_name }}</td>
									<td>{{ $repository->event->category }}</td>
									<td>{{ $repository->event->expositor }}</td>
									<td>{{ $repository->created_at }}</td>
									<td>
										<a href="{{ asset('repositorios/' . $repository->file_name) }}" style="color: olivedrab; cursor: pointer;">
											<i class="btn-download fa fa-download fa-2x"></i>
										</a>
									</td>
									@if (Auth::user()->role < 2 )
										<td>
											<button type="button" class="btn btn-primary btn-eliminar" value ="{{ $repository->id }}">
												Eliminar
											</button>
										</td>
									@endif
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			@endif
		</div>
	</div>
@endsection

@section('script')
	<script>
		$(document).ready(function(){
			console.log('ready');
			
			$('.btn-eliminar').click(function(e) {
				window.location = "/mi/evento/repositorio/eliminar/" + $(this).val()
			});
		});
	</script>
@endsection
