{{-- resources/views/common/flash.blade.php --}}

@if (session()->has('message'))
	<div class="alert {{ session()->has('important') ? 'alert-info alert-important' : 'alert-success' }}">
		@if (session()->has('important'))
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		@endif
		{{ session()->get('message') }}
	</div>
@endif