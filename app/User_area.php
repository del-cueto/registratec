<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class User_area extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'area_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];
	
	/**
     * Get the user the area belongs to.
     */
    public function user()
    {
         return $this->belongsTo('App\User');
    }
	
	/**
     * Get the area the user belongs to.
     */
    public function area()
    {
         return $this->belongsTo('App\Area');
    }
}
