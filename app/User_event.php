<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class User_event extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'event_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];
	
	/**
     * Get the user the area belongs to.
     */
    public function user()
    {
         return $this->belongsTo('App\User');
    }
	
	/**
     * Get the event the user belongs to.
     */
    public function event()
    {
         return $this->belongsTo('App\Event');
    }
}
