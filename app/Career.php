<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Career extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];
	
	/**
     * Get all of the user_careers for the area.
     */
    public function user_careers()
    {
         return $this->hasMany('App\User_career');
    }
}
