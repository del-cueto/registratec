<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'street', 'street_number','colony', 'postal_number', 'city','state', 'country', 'phone', 'date_start', 'date_end', 'hour_start', 'hour_end', 'category', 'expositor', 'enrolled', 'image_name', 'map_name', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];
	
	/**
     * Get all of the user_events for the event.
     */
    public function user_events()
    {
         return $this->hasMany('App\User_event');
    }
	
	/**
     * Get all of the repositories for the event.
     */
    public function repositories()
    {
         return $this->hasMany('App\Repository');
    }
}
