<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\User;

class RedirectIfAuthenticated {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null) {
		$user = User::where('email', $request->email)
			->first();
		
		if ($user) {
			if ($user->status == 0) {
				return redirect('/login')->with('message', 'Te haz registrado exitosamente. En un periodo de 48 horas podras tener acceso al sitio.');
			}
		}

        if (Auth::guard($guard)->check()) {
            return redirect('/login');
        }

        return $next($request);
    }
}
