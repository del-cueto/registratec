<?php

namespace App\Http\Controllers;

use App\Event;
use App\Gallery;
use App\User_event;
use App\Repository;
use App\User;
use App\Area;
use App\Career;
use App\User_Area;
use App\User_Career;
use Validator;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$dt = \Carbon\Carbon::now();
		$dt->subDays(10);
		$galleries = Gallery::where('updated_at', '>=', $dt)
			->get();
		return view("home", compact("galleries"));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function welcome()
    {
		if (\Auth::user()->status == 0) {
			\Auth::logout();
			session()->flash('message', 'Te haz registrado exitosamente. En un periodo de 48 horas podras tener acceso al sitio.');
			return redirect('/login')->with('message', 'Te haz registrado exitosamente. En un periodo de 48 horas podras tener acceso al sitio.');
		}
		return view("welcome");
    }

    /**
     * Show the application events.
     *
     * @return \Illuminate\Http\Response
     */
    public function events()
    {
		if (\Auth::user()->role > 1) {
			$events = Event::where('status', 1)->get();
		} else {
			$events = Event::all();
		}
		
		foreach ($events as $event) {
			$event->assigned = false;
			$event_assigned = User_Event::where('user_id', \Auth::user()->id)
				->where('event_id', $event->id)
				->get()->all();
			if ($event_assigned) {
				$event->assigned = true;
			}
		}
		
		return view("layouts.events.events", compact("events"));
    }

    /**
     * Show the application events.
     *
     * @return \Illuminate\Http\Response
     */
    public function searchEvents($user_id)
    {
		$user = User::find($user_id);
		$user_events = collect();
		foreach ($user->user_events as $event) {
			$user_events->push($event->event);
		}
		$events = Event::where('status', 1)->get();
		$intersected_events = $events->diff($user_events);
		$intersected_events->all();
		
		return response()->json([
			'eventos' => $intersected_events,
		]);
    }

    /**
     * Show the application resources.
     *
     * @return \Illuminate\Http\Response
     */
    public function resources()
    {
		$events = Event::all();
		return view("layouts.events.events", compact("events"));
    }

    /**
     * Show the application registers.
     *
     * @return \Illuminate\Http\Response
     */
    public function register()
    {
		$events = Event::all();
		return view("home", compact("events"));
    }

    /**
     * Show the application registers.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewRegisterEvents()
    {
		return view("layouts.register.registerevents");
    }

    /**
     * Show the application registers.
     *
     * @return \Illuminate\Http\Response
     */
    public function registerEvents(Request $request)
    {
		$this->validate($request, [
            'name' => 'required|max:255',
            'street' => 'required|max:255',
            'city' => 'required|max:255',
            'date_start' => 'required|max:255',
            'date_end' => 'required|max:255',
            'hour_start' => 'required|max:255',
            'hour_end' => 'required|max:255',
			'phone' => 'numeric',
            'expositor' => 'required|max:255',
            'enrolled' => 'required|max:255',
        ]);
		
		
		$name = $request->name;
		$street = $request->street;
		$street_number = $request->street_number;
		$colony = $request->colony;
		$postal_number = $request->postal_number;
		$city = $request->city;
		$state = $request->state;
		$country = $request->country;
		$phone = $request->phone;
		$date_start = $request->date_start;
		$date_end = $request->date_end;
		$hour_start = $request->hour_start;
		$hour_end = $request->hour_end;
		$category = $request->category;
		$expositor = $request->expositor;
		$enrolled = $request->enrolled;
		
		$destinationPath = base_path('public') . "\\eventos";
		$image_name = '';
		$map_name = '';
		
		if ($request->hasFile('image_name')) {
			if ($request->file('image_name')->isValid()) {
				$fileName = $request->file('image_name')->getClientOriginalName();
				$filePath = $request->file('image_name')->getRealPath();
				$fileSize = $request->file('image_name')->getClientSize();
				$image_name = $fileName;
				
				$request->file('image_name')->move($destinationPath, $fileName);
			}
		}
		
		if ($request->hasFile('map_name')) {
			if ($request->file('map_name')->isValid()) {
				$fileName = $request->file('map_name')->getClientOriginalName();
				$filePath = $request->file('map_name')->getRealPath();
				$fileSize = $request->file('map_name')->getClientSize();
				$map_name = $fileName;
				
				$request->file('map_name')->move($destinationPath, $fileName);
			}
		}
				
		Event::create([
			'name' => $name,
			'street' => $street,
			'street_number' => $street_number,
			'colony' => $colony,
			'postal_number' => $postal_number,
			'city' => $city,
			'state' => $state,
			'country' => $country,
			'phone' => $phone,
			'date_start' => $date_start,
			'date_end' => $date_end,
			'hour_start' => $hour_start,
			'hour_end' => $hour_end,
			'category' => $category,
			'expositor' => $expositor,
			'enrolled' => $enrolled,
			'image_name' => $image_name,
			'map_name' => $map_name,
			'map_name' => 0,
			'status' => 1,
		]);
		
	    $request->session()->flash('mensaje', 'Evento registrado satisfactoriamente!');

		return redirect('/events');
    }

    /**
     * Show the application register assistants.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewRegisterAssistants()
    {
		$users = User::where('role', '>', '0')->get();
		
		foreach ($users as $user) {
			$user->career = $user->user_careers()->orderBy('id', 'desc')->first();
			$user->area = $user->user_areas()->orderBy('id', 'desc')->first();
		}
		
		return view("layouts.register.registerassistants", compact('users'));
    }

    /**
     * Register assistants for the system.
     *
     * @return \Illuminate\Http\Response
     */
    public function registerAssistants(Request $request, $user)
    {
		$user = User::find($user);
		
		if ($user->role == 1) {
			$user->role = 2;
		} else {
			$user->role = 1;
		}
		
		$user->save();
		
	    $request->session()->flash('mensaje', 'Asistente asignado satisfactoriamente!');

		return back();
    }

    /**
     * Show the application register users.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewRegisterUsers()
    {
		$users = User::where('role', '>', '1')->get();
		
		foreach ($users as $user) {
			$user->career = $user->user_careers()->orderBy('id', 'desc')->first();
			$user->area = $user->user_areas()->orderBy('id', 'desc')->first();
		}
		
		return view("layouts.register.registerusers", compact('users'));
    }

    /**
     * Show the application register new users.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewRegisterNewUsers()
    {
		$areas = Area::all();
		$careers = Career::all();
		return view("layouts.register.registernewusers", compact('areas', 'careers'));
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    public function registerNewUsers(Request $request)
    {
		$this->validate($request, [
            'name' => 'required|max:255',
            'first_last_name' => 'required|max:255',
            'institute' => 'required|max:255',
            'email' => 'required|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
			'control_number' => 'min:8|unique:users',
			'clock_number' => 'unique:users',
			'ine_id' => 'min:13|unique:users',
        ]);
		
		$area_id = $request->area;
		$career_id = $request->career;
		
		$user = User::firstOrCreate([
            'name' => ucfirst($request->name),
            'first_last_name' => ucfirst($request->first_last_name),
            'second_last_name' => ucfirst($request->second_last_name),
            'institute' => $request->institute,
            'control_number' => $request->control_number,
            'clock_number' => $request->clock_number,
            'ine_id' => $request->ine_id,
            'status' => 1,
            'role' => $request->role,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]);
		
		if (isset($user) && $area_id) {
			User_area::firstOrCreate([
				'user_id' => $user->id,
				'area_id' => $area_id,
			]);
		}
		
		if (isset($user) && $career_id) {
			User_career::firstOrCreate([
				'user_id' => $user->id,
				'career_id' => $career_id,
			]);
		}
		
        return redirect('/');
    }

    /**
     * Show the application register new users.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewRegisterModifyUsers()
    {
		$users = User::where('role', '>', '0')->get();
		
		foreach ($users as $user) {
			$user->careers = $user->user_careers()->orderBy('id', 'desc')->first();
			if ($user->careers) {
				$user->career = $user->careers->career->name;
			}
			$user->areas = $user->user_areas()->orderBy('id', 'desc')->first();
			if ($user->areas) {
				$user->area = $user->areas->area->name;
			}
		}
		
		return view("layouts.register.registermodifyusers", compact('users'));
    }

    /**
     * Show the application register new users.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewRegisterModifyUser($user_id)
    {
		$user = User::find($user_id);
		
		$user->careers = $user->user_careers()->orderBy('id', 'desc')->first();
		if ($user->careers) {
			$user->career = $user->careers->career->name;
		}
		$user->areas = $user->user_areas()->orderBy('id', 'desc')->first();
		if ($user->areas) {
			$user->area = $user->areas->area->name;
		}
		
		$areas = Area::all();
		$careers = Career::all();
		
		return view("layouts.register.registermodifyuser", compact('user', 'areas', 'careers'));
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    public function registerModifyUser(Request $request, $user_id)
    {
		$user = User::find($user_id);
		
		if ($user) {
			$this->validate($request, [
				'name' => 'required|max:255',
				'first_last_name' => 'required|max:255',
				'password' => 'required|min:6|confirmed',
			]);
			
			$area_id = $request->area;
			$career_id = $request->career;
			
			$user->name = $request->name;
			$user->first_last_name = $request->first_last_name;
			$user->second_last_name = $request->second_last_name;
			$user->institute = $request->institute;
			$user->control_number = $request->control_number;
			$user->clock_number = $request->clock_number;
			$user->ine_id = $request->ine_id;
			$user->role = $request->role;
			$user->password = bcrypt($request->password);
			
			$user->save();
			
			if (isset($user) && $area_id) {
				User_area::firstOrCreate([
					'user_id' => $user->id,
					'area_id' => $area_id,
				]);
			}
			
			if (isset($user) && $career_id) {
				User_career::firstOrCreate([
					'user_id' => $user->id,
					'career_id' => $career_id,
				]);
			}
		}
		
        return redirect('/registrar/modificar/usuarios');
    }

    /**
     * Show the application register new users.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewRegisterUserEvent()
    {
		$users = User::where('role', '>', '1')->get();

		foreach ($users as $user) {
			$user->careers = $user->user_careers()->orderBy('id', 'desc')->first();
			if ($user->careers) {
				$user->career = $user->careers->career->name;
			}
			$user->areas = $user->user_areas()->orderBy('id', 'desc')->first();
			if ($user->areas) {
				$user->area = $user->areas->area->name;
			}
		}
		
		return view("layouts.register.registeruserevent", compact('users'));
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    public function registerUserEvent(Request $request)
    {
		$user = User::find($user_id);
		
		if ($user) {
			$this->validate($request, [
				'name' => 'required|max:255',
				'first_last_name' => 'required|max:255',
				'password' => 'required|min:6|confirmed',
			]);
			
			$area_id = $request->area;
			$career_id = $request->career;
			
			$user->name = $request->name;
			$user->first_last_name = $request->first_last_name;
			$user->second_last_name = $request->second_last_name;
			$user->institute = $request->institute;
			$user->control_number = $request->control_number;
			$user->clock_number = $request->clock_number;
			$user->ine_id = $request->ine_id;
			$user->role = $request->role;
			$user->password = bcrypt($request->password);
			
			$user->save();
			
			if (isset($user) && $area_id) {
				User_area::firstOrCreate([
					'user_id' => $user->id,
					'area_id' => $area_id,
				]);
			}
			
			if (isset($user) && $career_id) {
				User_career::firstOrCreate([
					'user_id' => $user->id,
					'career_id' => $career_id,
				]);
			}
		}
		
        return redirect('/registrar/usuarios/eventos');
    }

    /**
     * Register assistants for the system.
     *
     * @return \Illuminate\Http\Response
     */
    public function registerAproveRejectUser(Request $request, $user)
    {
		$user = User::find($user);

		if ($user->status == 1) {
			$user->status = 0;
		} else {
			$user->status = 1;
		}
		
		$user->save();
		
	    $request->session()->flash('mensaje', 'Asistente asignado satisfactoriamente!');

		return back();
    }

    /**
     * Show the application registers.
     *
     * @return \Illuminate\Http\Response
     */
    public function uploadGalleryImage(Request $request)
    {
		$this->validate($request, [
			'image' => 'required|max:10000',
		]);
		
		$destinationPath = base_path('public') . @"\galeria";
				
		if ($request->hasFile('image')) {
			if ($request->file('image')->isValid()) {
				$fileName = $request->file('image')->getClientOriginalName();
				$filePath = $request->file('image')->getRealPath();
				$fileSize = $request->file('image')->getClientSize();
				
				$request->file('image')->move($destinationPath, $fileName);
				
				Gallery::create([
					'image_name' => $fileName,
				]);
			}
		}
		
	    $request->session()->flash('mensaje', 'Imagen cargada satisfactoriamente!');

		return redirect('/galerias');
    }
	
	

    /**
     * Show the application registers.
     *
     * @return \Illuminate\Http\Response
     */
    public function removeGallery($gallery_id)
    {
		$gallery = Gallery::find($gallery_id);
		
		$gallery->delete();

		return back();
    }

    /**
     * Activate or deactivate events.
     *
     * @return \Illuminate\Http\Response
     */
    public function actideactivate($event)
    {
		$event = Event::find($event);
		try {
			if ($event->status) {
				$event->status = 0;
			} else {
				$event->status = 1;
			}
			$event->save();
			//dd($event);
			
			return response()->json([
				'success' => true
			]);
		} catch (\Exception $e) {
			return response()->json([
				'success' => false,
				'error' => 'problemas al activar o desactivar'
			]);
		}
    }

    /**
     * Assign or cancel assigment to events.
     *
     * @return \Illuminate\Http\Response
     */
    public function assigncancel($event)
    {
		$event = Event::find($event);
		try {
			$event_assigned = User_event::where('user_id', \Auth::user()->id)
				->where('event_id', $event->id)
				->first();
			if ($event_assigned) {
				$assign = User_event::firstOrCreate([
					'user_id' => \Auth::user()->id,
					'event_id' => $event->id,
				]);
				$assign->delete();
			} else {
				User_event::firstOrCreate([
					'user_id' => \Auth::user()->id,
					'event_id' => $event->id,
				]);
			}
			
			return response()->json([
				'success' => true
			]);
		} catch (\Exception $e) {
			return response()->json([
				'success' => false,
				'error' => 'problemas al asignar o cancelar'
			]);
		}
    }

    /**
     * Assign or cancel assigment to events.
     *
     * @return \Illuminate\Http\Response
     */
    public function assignEvent(Request $request)
    {
		$this->validate($request, [
			'event_id' => 'required',
			'user_id' => 'required',
		]);
		$event = Event::find($request->event_id);
		$user = User::find($request->user_id);
		try {
			$event_assigned = User_event::where('user_id', $user->id)
				->where('event_id', $event->id)
				->first();
			if ($event_assigned) {
				$assign = User_event::firstOrCreate([
					'user_id' => $user->id,
					'event_id' => $event->id,
				]);
				$assign->delete();
			} else {
				User_event::firstOrCreate([
					'user_id' => $user->id,
					'event_id' => $event->id,
				]);
			}
			
			return back();
		} catch (\Exception $e) {
			return response()->json([
				'success' => false,
				'error' => 'problemas al asignar o cancelar'
			]);
		}
    }

    /**
     * search for selected event.
     *
     * @return \Illuminate\Http\Response
     */
    public function searchEvent($event)
    {
		$event = Event::find($event);

		return $event;
    }

    /**
     * delete for selected event.
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteEvent($event)
    {
		$event = Event::find($event);
		$event->delete();

		return back();
    }

    /**
     * Display the events assigned to the user.
     *
     * @return \Illuminate\Http\Response
     */
    public function myEvents(Request $request)
    {
		$dt = \Carbon\Carbon::now();
		$events = collect();
		$assisted_events = collect();
		$user = \Auth::user();
		$user_events = $user->user_events->where('date_start', '>', $dt);
		$user_assisted_events = $user->user_events->where('date_start', '<=', $dt);
		foreach ($user_events as $user_event) {
			$events->push($user_event->event);
		}
		foreach ($user_assisted_events as $user_event) {
			$assisted_events->push($user_event->event);
		}
		
		return view("layouts.events.miseventos", compact("events", "assisted_events"));
    }

    /**
     * Display the events repository for the user's event.
     *
     * @return \Illuminate\Http\Response
     */
    public function repository(Request $request, $event)
    {
		$event = Event::find($event);
		
		$search = $request->search;
		if ($search) {
			$repositories = $event->repositories()->where('file_name', 'like', '%' . $search . '%')->get();
		} else {
			$repositories = $event->repositories;
		}

		return view("layouts.events.repository", compact("repositories", "event"));
    }

    /**
     * Add repository to the event.
     *
     * @return \Illuminate\Http\Response
     */
    public function addRepository(Request $request, $event)
    {
		$this->validate($request, [
            'file_name' => 'required',
        ]);
		
		$file_name = $request->file_name;
		
		$destinationPath = base_path('public') . "\\repositorios";
		
		if ($request->hasFile('file_name')) {
			if ($request->file('file_name')->isValid()) {
				$fileName = $request->file('file_name')->getClientOriginalName();
				$filePath = $request->file('file_name')->getRealPath();
				$fileSize = $request->file('file_name')->getClientSize();
				$file_name = $fileName;
				
				$request->file('file_name')->move($destinationPath, $fileName);
			}
		}

		$event = Event::find($event);
		
		Repository::firstOrCreate([
			'event_id' => $event->id,
			'file_name' => $file_name,
		]);
		
		$request->session()->flash('mensaje', 'Repositorio agregado satisfactoriamente!');

		return redirect('mi/evento/repositorio/' . $event->id);
    }

    /**
     * Delete repository to the event.
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteRepository(Request $request, $repository)
    {
		$repository = Repository::find($repository);
		if ($repository) {
			$repository->delete();
		}

		return back();
    }
	
    /**
     * Display the events repository for all events.
     *
     * @return \Illuminate\Http\Response
     */
    public function repositories(Request $request)
    {
		$search = $request->search;
		if ($search) {
			$repositories = Repository::where('file_name', 'like', '%' . $search . '%')->get();
		} else {
			$repositories = Repository::all();
		}

		return view("layouts.events.repositories", compact("repositories"));
    }
	
    /**
     * Display the events repository for all events.
     *
     * @return \Illuminate\Http\Response
     */
    public function addCareer(Request $request)
    {
		$this->validate($request, [
            'addCareer' => 'required'
        ]);
		
		Career::firstOrCreate([
			'name' => $request->addCareer
		]);
		
		return back();
    }
	
    /**
     * Display the events repository for all events.
     *
     * @return \Illuminate\Http\Response
     */
    public function addArea(Request $request)
    {
		$this->validate($request, [
            'addArea' => 'required'
        ]);
		
		Area::firstOrCreate([
			'name' => $request->addArea
		]);
		
		return back();
    }
	
    /**
     * Display the events repository for all events.
     *
     */
    public function eventsReport()
    {
		$users = User::all();
		return view('layouts.events.report', compact('users'));
    }
	
    /**
     * Display the events repository for all events.
     *
     */
    public function modifyEvent(Request $request, $event_id)
    {
		$this->validate($request, [
            'name' => 'required',
            'date_start' => 'required',
            'date_start' => 'required',
            'hour_start' => 'required',
            'hour_end' => 'required',
            'enrolled' => 'required',
            'expositor' => 'required',
        ]);
		
		$event = Event::find($event_id);
		
		$destinationPath = base_path('public') . "\\eventos";
		$image_name = '';
		
		if ($request->hasFile('image')) {
			if ($request->file('image')->isValid()) {
				$fileName = $request->file('image')->getClientOriginalName();
				$filePath = $request->file('image')->getRealPath();
				$fileSize = $request->file('image')->getClientSize();
				$image_name = $fileName;
				
				$request->file('image')->move($destinationPath, $fileName);
				$event->image_name = $image_name;
			}
		}
		
		$event->name = $request->name;
		$event->category = $request->category;
		$event->date_start = $request->date_start;
		$event->date_end = $request->date_end;
		$event->hour_start = $request->hour_start;
		$event->hour_end = $request->hour_end;
		$event->enrolled = $request->enrolled;
		$event->expositor = $request->expositor;
		
		$event->save();
		
		return back();
    }
	
    /**
     * Display the assistance to events.
     *
     */
    public function assistance()
    {
		$events = Event::all();
		
		return view("layouts.events.assistance", compact("events"));
    }
	
    /**
     * Display the event assistance.
     *
     */
    public function eventAssistance(Request $request, $event)
    {
		$event = Event::find($event);
		$user_events = $event->user_events;
		return view("layouts.events.eventassistance", compact("event", "user_events"));
    }
}
