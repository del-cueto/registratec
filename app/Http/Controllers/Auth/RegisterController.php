<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Area;
use App\Career;
use App\User_area;
use App\User_career;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'first_last_name' => 'required|max:255',
            'institute' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
			'control_number' => 'min:8|unique:users',
			'clock_number' => 'unique:users',
			'ine_id' => 'min:13|unique:users',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
		$area_id = $data['area'];
		$career_id = $data['career'];
		
		$user = User::create([
            'name' => ucfirst($data['name']),
            'first_last_name' => ucfirst($data['first_last_name']),
            'second_last_name' => ucfirst($data['second_last_name']),
            'institute' => $data['institute'],
            'control_number' => $data['control_number'],
            'clock_number' => $data['clock_number'],
            'ine_id' => $data['ine_id'],
            'status' => 0,
            'role' => $data['role'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);

		if (isset($user) && $area_id) {
			User_area::firstOrCreate([
				'user_id' => $user->id,
				'area_id' => $area_id,
			]);
		}

		if (isset($user) && $career_id) {
			User_career::firstOrCreate([
				'user_id' => $user->id,
				'career_id' => $career_id,
			]);
		}

		//flash()->success('Te haz registrado exitosamente. En un periodo de 48 horas podras tener acceso al sitio.');
		session()->flash('message', 'Te haz registrado exitosamente. En un periodo de 48 horas podras tener acceso al sitio.');
		
        return $user;
    }
	
	public function showRegistrationForm()
	{
		$areas = Area::all();
		$careers = Career::all();
		return view("auth.register", compact("areas", "careers"));
	}
}
